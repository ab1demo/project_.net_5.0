﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project_.net_5._0.BusinessLogic
{
    public class MathBL
    {
        public int Sum(int num1, int num2)
        {
            int sum = num1 + num2;
            return sum;
        }
    }
}
